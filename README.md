# CoReS - CoronaRechtsSammlung

CoReS ist ein Projekt zur Erstellung einer Webseite, auf der deutschlandweit alle Regelungen, Beschlüsse, Maßnahmen und Erlässe zur COVID-19-Pandemie gut sortiert und durchsuchbar gesammelt werden sollen. Die Regelungen werden nach räumlichen Gebieten, Metadaten wie Geltungszeiträumen und mittels Schlagwörtern nach Sachbereichen sortiert, um die jeweils gültigen Regelungen zu finden und einen Vergleich zu ermöglichen. Es werden auch Bezüge zwischen Maßnahmen abgebildet, sodass etwa die Aufhebung einer solchen ersichtlich wird.

## Hackathon

Dieses Projekt ist im Rahmen des [WirVsVirusHackathon](https://wirvsvirushackathon.org/) der Bundesregierung vom 20. bis 22. März 2020 entstanden. In diesem Zusammenhang findet sich eine längere Erläuterung des Projekts auf [Devpost](https://devpost.com/software/1_039_b_staatliche_kommunikation_citcomm).

# License

Dieses Projekt ist lizensiert unter der European Union Public Licence (EUPL) Version 1.2 oder neuer (ABl. EU L 128, 17. Mai 2017, S. 59; eupl.eu). Gemäß dieser Lizenz wird keine Gewährleistung gewährt und ein Haftungsausschluss (außer für Vorsatz und Personenschäden) vereinbart.

This project is licensed under the European Union Public Licence (EUPL) version 1.2 or later (OJEU, L 128, 19 May 2017, p. 59; eupl.eu). 
According to this license it comes without warranties and the Licensor will in no event be liable, except in the cases of wilful misconduct or damages directly caused to natural persons.
